
### Keyboard Layout Pegon
Tata letak papan tombol aksara Pegon.


## Instalasi

1. Unduh `Pegon.dmg`
2. Buka `Pegon.dmg`
3. Pindahkan `Pegon.bundle` ke `Drag here to install`.
4. Tambahkan keyboard dengan membuka `System Preferences > Keyboard > Input Source`. Cari `Javanese`, Pilih `Keyboard Pegon`.
